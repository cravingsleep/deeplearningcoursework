# turns normal candlesticks into Heiken-Ashi
def heiken_ashi(candlesticks):
    # add in the first candlestick with normal calcs
    candlesticks[0]['xClose'] = \
        (candlesticks[0]['closePrice'] +
         candlesticks[0]['openPrice'] +
         candlesticks[0]['highPrice'] +
         candlesticks[0]['lowPrice']) / 4

    candlesticks[0]['xOpen'] = \
        (candlesticks[0]['closePrice'] + candlesticks[0]['openPrice']) / 2

    # calculate for each other candlestick
    for index in range(1, len(candlesticks)):
        previous = candlesticks[index - 1]
        current = candlesticks[index]

        open_price, close_price = current['openPrice'], current['closePrice']
        low_price, high_price = current['lowPrice'], current['highPrice']

        current['xClose'] = (open_price + close_price + low_price + high_price) / 4
        current['xOpen'] = (previous['xClose'] + previous['xOpen']) / 2
        current['xHigh'] = max(high_price, current['xOpen'], current['xClose'])
        current['xLow'] = min(low_price, current['xOpen'], current['xClose'])
