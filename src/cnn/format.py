import numpy as np


def xClose_volume_rsi_history_colour(candlestick):
    return np.array([candlestick['xClose'],
                     candlestick['volume'],
                     candlestick['rsi'],
                     candlestick['history'],
                     candlestick['colour']])


def format_to_class(candlestick, heiken=False):
    if heiken:
        return np.array([candlestick['xisNextGreen']])
    else:
        return np.array([candlestick['isNextGreen']])


# calculates whether the next candlesticks is heiken-ashi green
def heiken_nexts(candlesticks):
    for index in range(0, len(candlesticks)):
        current = candlesticks[index]

        if index == len(candlesticks) - 1:
            current['xisNextGreen'] = 0.0
        else:
            future = candlesticks[index + 1]

            if future['xClose'] >= future['xOpen']:
                current['xisNextGreen'] = 1.0
            else:
                current['xisNextGreen'] = 0.0
