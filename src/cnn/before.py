from util import isHeikenAshiGreen, isGreen


# places a colour on all the candles
def green_or_red(candlesticks, heiken):
    green_func = isHeikenAshiGreen if heiken else isGreen

    for candlestick in candlesticks:
        candlestick['colour'] = 1.0 if green_func(candlestick) else 0.0

# calcs how many red and green candles in the history (periods back)
# 1 if predom green, 0 otherwise
def colour_history(candlesticks, heiken, periods=10):
    # for normalization
    max_candles = 0

    # set the isGreen? function
    green_func = isHeikenAshiGreen if heiken else isGreen

    # for each candle
    for index in range(0, len(candlesticks)):
        reds = 0
        greens = 0

        for before_index in range(index - 1, max(index - 1 - periods, 0), -1):
            if green_func(candlesticks[before_index]):
                greens += 1
            else:
                reds += 1

        if greens >= reds:
            candlesticks[index]['history'] = 1.0
        else:
            candlesticks[index]['history'] = 0.0

