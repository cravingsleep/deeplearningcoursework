def isGreen(candlestick):
    return candlestick['closePrice'] >= candlestick['openPrice']


def isHeikenAshiGreen(candlestick):
    return candlestick['xClose'] >= candlestick['xOpen']