# places a Relative Strength Index on all candles periods:len
def rsi_all(candlesticks, periods=14):
    # all the closePrices in a list [3933, 3934, ...]
    closes = list(map(lambda candlestick: candlestick['closePrice'], candlesticks))

    # take the initial candlesticks
    starters = closes[0:periods]

    # zip the list together with its tail so we get [(3933, 3934), ...]
    tied = list(zip(starters, starters[1: len(starters)]))

    starting_gains = [gain(pair) for pair in tied if gain(pair)]
    starting_losses = [loss(pair) for pair in tied if loss(pair)]

    first_average_gain = sum(starting_gains) / 14
    first_average_loss = sum(starting_losses) / 14

    first_relative_strength = first_average_gain / first_average_loss
    first_rsi = norm_rsi(first_relative_strength)

    candlesticks[periods]['rsi'] = first_rsi

    average_gain = first_average_gain
    average_loss = first_average_loss

    for index in range(periods + 1, len(candlesticks)):
        previous = candlesticks[index - 1]
        current = candlesticks[index]

        price_delta = current['closePrice'] - previous['closePrice']
        if price_delta >= 0.0:
            average_gain = (13 * average_gain + price_delta) / 14
        else:
            average_loss = (13 * average_loss + abs(price_delta)) / 14

        rs = average_gain / average_loss
        current['rsi'] = norm_rsi(rs)


def gain(double_tuple):
    delta = double_tuple[1] - double_tuple[0]

    if delta >= 0.0:
        return delta
    else:
        return None


def loss(double_tuple):
    delta = double_tuple[1] - double_tuple[0]

    if delta < 0.0:
        return abs(delta)
    else:
        return None


def norm_rsi(rs):
    return 1 - (1 / (1 + rs))
