import json


def load_candlestick_data(file_name):
    with open(file_name) as json_file:
        data = json.load(json_file)
        json_file.close()
        return data['candlesticks'], data['maxes']

