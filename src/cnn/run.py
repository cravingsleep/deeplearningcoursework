import matplotlib.pyplot as plt
from keras.layers import Dense
from keras.models import Sequential
from format import *
from read_data import load_candlestick_data
from indicators import rsi_all
from heiken_ashi import heiken_ashi
from before import colour_history, green_or_red

# load the candlesticks from json file
candlestick_data, maxes = load_candlestick_data('../data/6hnov2nd_norm.json')

# randomise and load the data into a numpy array for conviencence
candlestick_data = np.array(candlestick_data)
np.random.shuffle(candlestick_data)

# calculate the Relative Strength Index for the candles
periods = 14
rsi_all(candlestick_data, periods)

# calculate the heiken ashi candles
heiken_ashi(candlestick_data)

# calculate the colour history
colour_history(candlestick_data, heiken=True, periods=4)

# calculate the colour of the candles
green_or_red(candlestick_data, heiken=True)

# calculate the string of heikens
heiken_nexts(candlestick_data)

# grab the data we need for each candle
all_data_inputs = \
    np.array([xClose_volume_rsi_history_colour(candlestick) for candlestick in candlestick_data[periods:]])

# obtain all [isNextGreen?]
all_data_outputs = \
    np.array([format_to_class(candlestick, heiken=True) for candlestick in candlestick_data[periods:]])

print('Total dataset has {} candlesticks.'.format(len(all_data_inputs)))

# split into training and validation
validation_size = 50

validation_inputs = all_data_inputs[0:validation_size]
validation_ouputs = all_data_outputs[0:validation_size]
training_inputs = all_data_inputs[validation_size:len(all_data_inputs)]
training_outputs = all_data_outputs[validation_size:len(all_data_inputs)]

# plot colours, volume, rsi against isNextGreen
# fig = plt.figure(figsize=(10, 10))
# ax = Axes3D(fig)
#
# green_inputs = []
# red_inputs = []
#
# for index in range(len(training_inputs)):
#     output = training_outputs[index]
#
#     if output[0] == 1:
#         green_inputs.append(training_inputs[index])
#     else:
#         red_inputs.append(training_inputs[index])
#
# green_inputs = np.array(green_inputs)
# red_inputs = np.array(red_inputs)
#
# print('Greens: {}'.format(len(green_inputs)))
# print('Reds: {}'.format(len(red_inputs)))
#
# # [closePrice, volume, sma] (plot closePrice, sma)
# ax.set_xlabel('Colour')
# ax.set_ylabel('Volume')
# ax.set_zlabel('RSI')
#
# ax.scatter(green_inputs[:, 4],
#            green_inputs[:, 1],
#            green_inputs[:, 2],
#            c='green')
#
# ax.scatter(red_inputs[:, 4],
#            red_inputs[:, 1],
#            red_inputs[:, 2],
#            c='red')

# plt.show()

# fig = plt.figure(figsize=(10, 10))
#
# green_inputs = []
# red_inputs = []
#
# for index in range(len(training_inputs)):
#     output = training_outputs[index]
#
#     if output[0] == 1:
#         green_inputs.append(training_inputs[index])
#     else:
#         red_inputs.append(training_inputs[index])
#
# green_inputs = np.array(green_inputs)
# red_inputs = np.array(red_inputs)
#
# plt.xlabel('Close Price')
# plt.ylabel('Volume')
# plt.plot(green_inputs[:, 0],
#          green_inputs[:, 1],
#          'o',
#          color='green')
#
# plt.plot(red_inputs[:, 0],
#          red_inputs[:, 1],
#          'x',
#          color='red')
# plt.show()


# 3 -> 12 -> 6 -> 1
model = Sequential()
model.add(Dense(12, input_dim=training_inputs.shape[1], activation='tanh'))
model.add(Dense(6, activation='sigmoid'))
model.add(Dense(1, activation='sigmoid'))

model.compile(loss='mse', optimizer='adam', metrics=['mse', 'binary_accuracy'])

hist = model.fit(training_inputs, training_outputs, batch_size=10, epochs=200, verbose=2)

plt.figure(figsize=(10, 10))
plt.ylabel('Binary Accuracy')
plt.xlabel('Iteration')
plt.plot(hist.history['binary_accuracy'])
plt.show()

print(model.metrics_names)
print(model.evaluate(validation_inputs, validation_ouputs, verbose=0))
