const fs = require('fs');
const _ = require('lodash');

const filename = '../data/6hnov2nd';

const jsonObject = JSON.parse(fs.readFileSync(filename + '.json', 'utf8'));

const candlesticks = jsonObject.result['21600'];

const maxCloseTime = _.maxBy(candlesticks, c => c[0])[0];
const maxOpenPrice = _.maxBy(candlesticks, c => c[1])[1];
const maxHighPrice = _.maxBy(candlesticks, c => c[2])[2];
const maxLowPrice = _.maxBy(candlesticks, c => c[3])[3];
const maxClosePrice = _.maxBy(candlesticks, c => c[4])[4];
const maxVolume = _.maxBy(candlesticks, c => c[5])[5];

const normalized = _(candlesticks).initial().map((candlestick, index) => {
    const [closeTime, openPrice, highPrice, lowPrice, closePrice, volume] = candlestick;
    const nextCandlestick = candlesticks[index + 1];

    // if close is gt than open then it's green
    const isGreen = nextCandlestick[4] >= nextCandlestick[1];

    return {
        closeTime: closeTime / maxCloseTime,
        openPrice: openPrice / maxOpenPrice,
        highPrice: highPrice / maxHighPrice,
        lowPrice: lowPrice / maxLowPrice,
        closePrice: closePrice / maxClosePrice,
        volume: volume / maxVolume,
        isNextGreen: isGreen ? 1 : 0
    };
});

const answer = JSON.stringify({
    candlesticks: normalized,
    maxes: {
        "maxCloseTime": maxCloseTime,
        "maxOpenPrice": maxOpenPrice,
        "maxHighPrice": maxHighPrice,
        "maxLowPrice": maxLowPrice,
        "maxClosePrice": maxClosePrice,
        "maxVolume": maxVolume
    }
}, null, 2);

fs.writeFileSync(filename + '_norm.json', answer);

